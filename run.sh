#!/bin/bash

. common/log.sh

REPEAT_LIMIT=3
ROOTDIR="$(pwd)"

export REPEAT_LIMIT
export ROOTDIR

# Setup
setLogName "$(pwd)/log"
resetLog

if [ $# -ne 1 ]; then
    echo "invalid argument"
    exit 1
fi

. $1


#!/bin/bash

TEST_NAME=""
TEST_START_AT=0
TEST_DONE_AT=0


CYCLE_NUM=0
CYCLE_START_AT=0
CYCLE_DONE_AT=0

STEP_NAME=""
STEP_START_AT=0
STEP_DONE_AT=0
LOGFILE=log

fail()
{
    ts="$(getUtcTime)"
    echo "----------------------------------------------" | tee -a "$LOGFILE"
    echo "[$ts] FAIL $1" | tee -a "$LOGFILE"
    echo "----------------------------------------------" | tee -a "$LOGFILE"
    echo "      Test: $TEST_NAME" | tee -a "$LOGFILE"
    echo "      Cycle: $CYCLE_NUM" | tee -a "$LOGFILE"
    echo "      Step: $STEP_NAME" | tee -a "$LOGFILE"
    echo "----------------------------------------------" | tee -a "$LOGFILE"
    exit 1
}

resetLog()
{
    echo "" > "$LOGFILE"
}

getUtcTime()
{
    ts=$(date -u +%s)
    echo "$ts"
}

setLogName()
{
    LOGFILE=$1
}

startTest()
{
    ts="$(getUtcTime)"
    TEST_NAME="$1"
    TEST_START_AT=$ts
    echo "----------------------------------------------" | tee -a "$LOGFILE"
    echo "[$ts] Test '$TEST_NAME' started" | tee -a "$LOGFILE"
    echo "----------------------------------------------" | tee -a "$LOGFILE"
    echo "Host:      $(hostname)" | tee -a "$LOGFILE"
    echo "System:    $(uname -a)" | tee -a "$LOGFILE"
    echo "CPU:       $(cat /proc/cpuinfo | grep 'model name' | uniq)" | tee -a "$LOGFILE"
    echo "----------------------------------------------" | tee -a "$LOGFILE"
    echo "" | tee -a "$LOGFILE"
}

finishTest()
{
    ts="$(getUtcTime)"
    TEST_DONE_AT=$ts
    local duration=$((TEST_DONE_AT-TEST_START_AT))
    echo "----------------------------------------------" | tee -a "$LOGFILE"
    echo "[$ts] TEST '$TEST_NAME' done :: $duration (s)" | tee -a "$LOGFILE"
    echo "----------------------------------------------" | tee -a "$LOGFILE"
}

startCycle()
{
    CYCLE_NUM=$((CYCLE_NUM+1)) 
    ts="$(getUtcTime)"
    echo "----------------------------------------------" | tee -a "$LOGFILE"
    echo "[$ts] Cycle $CYCLE_NUM started" | tee -a "$LOGFILE"
    echo "----------------------------------------------" | tee -a "$LOGFILE"
    CYCLE_START_AT=$ts
}

finishCycle()
{
    ts="$(getUtcTime)"
    CYCLE_DONE_AT=$ts
    local duration=$((CYCLE_DONE_AT-CYCLE_START_AT))
    echo "----------------------------------------------" | tee -a "$LOGFILE"
    echo "[$ts] Cycle $CYCLE_NUM done :: duration $duration (s)" | tee -a "$LOGFILE"
    echo "----------------------------------------------" | tee -a "$LOGFILE"
    echo "" | tee -a "$LOGFILE"

}

startStep()
{
    ts="$(getUtcTime)"
    STEP_NAME="$1"
    STEP_START_AT=$ts
    echo "[$ts] Step '$STEP_NAME' started" | tee -a "$LOGFILE"
}

finishStep()
{
    ts="$(getUtcTime)"
    STEP_DONE_AT=$ts
    local duration=$((STEP_DONE_AT-STEP_START_AT))
    echo "[$ts] Step '$STEP_NAME' done :: duration $duration (s)" | tee -a "$LOGFILE"

}

saveLog()
{
    cp "$LOGFILE" "$1" > /dev/null
}
